FROM node:lts-alpine

# yinzhuoqun<zhuoqun527@qq.com>

WORKDIR /downloads

RUN npm install -g whistle \
    && npm i -g whistle.yzq-pipe

ARG port=8899

EXPOSE ${port}

CMD [ "whistle", "run", "--port", "${port}" ]
